﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillTree : MonoBehaviour
{
    int skillpoints; //set this to a high number for testing all the buttons

    int connectorCheck1;
    int connectorCheck2;
    int connectorCheck3;
    int connectorCheck4;

    public Text skillPointText;
    public Text skillsUnlockedText;
    public Text error;

    public Button TL1, TL2, TL3, TL4, TL5, TL6;
    public Button TM1, TM2, TM3, TM4, TM5, TM6;
    public Button TR1, TR2, TR3, TR4, TR5, TR6;  
    public Button BL1, BL2, BL3, BL4, BL5, BL6;
    public Button BM1, BM2, BM3, BM4, BM5, BM6;
    public Button BR1, BR2, BR3, BR4, BR5, BR6;
    public Button M1, M2, M3, M4, M5, M6;
    public Button C1, C2, C3, C4;


    //button to add skillpoints during testing
    public void PointAdd(){
        skillpoints++;
        skillPointText.text = ("Skill Points: " + skillpoints);
    }

    public void TopLeft(){
        if (TL1.interactable == true && skillpoints >= 1)
        {
            skillpoints--;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Rogue 1");
            error.text = ("");
            TL1.interactable = false;
            TL2.interactable = true;

        }
        else if (TL2.interactable == true && skillpoints >= 2)
        {
            skillpoints = skillpoints - 2;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Rogue 2");
            error.text = ("");
            TL2.interactable = false;
            TL3.interactable = true;

        }
        else if (TL3.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Rogue 3");
            error.text = ("");
            TL3.interactable = false;
            TL4.interactable = true;

        }
        else if (TL4.interactable == true && skillpoints >= 4)
        {
            skillpoints = skillpoints - 4;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Rogue 4");
            error.text = ("");
            TL4.interactable = false;
            TL5.interactable = true;

        }
        else if (TL5.interactable == true && skillpoints >= 5)
        {
            skillpoints = skillpoints - 5;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Rogue 5");
            error.text = ("");
            TL5.interactable = false;
            TL6.interactable = true;

        }
        else if (TL6.interactable == true && skillpoints >= 6)
        {
            skillpoints = skillpoints - 6;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Rogue 6");
            error.text = ("");
            TL6.interactable = false;
            connectorCheck1++;
            if(connectorCheck1 == 2)
            {
                C1.interactable = true;
            }
            
        }
        else
        {
            error.text = ("Requirements not met");
        }
    }

    public void TopMiddle()
    {
        if (TM1.interactable == true && skillpoints >= 1)
        {
            skillpoints--;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Ranger 1");
            error.text = ("");
            TM1.interactable = false;
            TM2.interactable = true;

        }
        else if (TM2.interactable == true && skillpoints >= 2)
        {
            skillpoints = skillpoints - 2;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Ranger 2");
            error.text = ("");
            TM2.interactable = false;
            TM3.interactable = true;

        }
        else if (TM3.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Ranger 3");
            error.text = ("");
            TM3.interactable = false;
            TM4.interactable = true;

        }
        else if (TM4.interactable == true && skillpoints >= 4)
        {
            skillpoints = skillpoints - 4;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Ranger 4");
            error.text = ("");
            TM4.interactable = false;
            TM5.interactable = true;

        }
        else if (TM5.interactable == true && skillpoints >= 5)
        {
            skillpoints = skillpoints - 5;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Ranger 5");
            error.text = ("");
            TM5.interactable = false;
            TM6.interactable = true;

        }
        else if (TM6.interactable == true && skillpoints >= 6)
        {
            skillpoints = skillpoints - 6;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Ranger 6");
            error.text = ("");
            TM6.interactable = false;
            connectorCheck1++;
            connectorCheck2++;
            if (connectorCheck1 == 2)
            {
                C1.interactable = true;
            }
            if (connectorCheck2 == 2)
            {
                C2.interactable = true;
            }

        }
        else
        {
            error.text = ("Requirements not met");
        }
    }

    public void TopRight()
    {
        if (TR1.interactable == true && skillpoints >= 1)
        {
            skillpoints--;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Knight 1");
            error.text = ("");
            TR1.interactable = false;
            TR2.interactable = true;

        }
        else if (TR2.interactable == true && skillpoints >= 2)
        {
            skillpoints = skillpoints - 2;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Knight 2");
            error.text = ("");
            TR2.interactable = false;
            TR3.interactable = true;

        }
        else if (TR3.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Knight 3");
            error.text = ("");
            TR3.interactable = false;
            TR4.interactable = true;

        }
        else if (TR4.interactable == true && skillpoints >= 4)
        {
            skillpoints = skillpoints - 4;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Knight 4");
            error.text = ("");
            TR4.interactable = false;
            TR5.interactable = true;

        }
        else if (TR5.interactable == true && skillpoints >= 5)
        {
            skillpoints = skillpoints - 5;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Knight 5");
            error.text = ("");
            TR5.interactable = false;
            TR6.interactable = true;

        }
        else if (TR6.interactable == true && skillpoints >= 6)
        {
            skillpoints = skillpoints - 6;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Knight 6");
            error.text = ("");
            TR6.interactable = false;
            connectorCheck2++;
            if (connectorCheck2 == 2)
            {
                C2.interactable = true;
            }

        }
        else
        {
            error.text = ("Requirements not met");
        }
    }

    public void BotRight()
    {
        if (BR1.interactable == true && skillpoints >= 1)
        {
            skillpoints--;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Brawler 1");
            error.text = ("");
            BR1.interactable = false;
            BR2.interactable = true;

        }
        else if (BR2.interactable == true && skillpoints >= 2)
        {
            skillpoints = skillpoints - 2;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Brawler 2");
            error.text = ("");
            BR2.interactable = false;
            BR3.interactable = true;

        }
        else if (BR3.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Brawler 3");
            error.text = ("");
            BR3.interactable = false;
            BR4.interactable = true;

        }
        else if (BR4.interactable == true && skillpoints >= 4)
        {
            skillpoints = skillpoints - 4;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Brawler 4");
            error.text = ("");
            BR4.interactable = false;
            BR5.interactable = true;

        }
        else if (BR5.interactable == true && skillpoints >= 5)
        {
            skillpoints = skillpoints - 5;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Brawler 5");
            error.text = ("");
            BR5.interactable = false;
            BR6.interactable = true;

        }
        else if (BR6.interactable == true && skillpoints >= 6)
        {
            skillpoints = skillpoints - 6;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Brawler 6");
            error.text = ("");
            BR6.interactable = false;
            connectorCheck4++;
            if (connectorCheck4 == 2)
            {
                C4.interactable = true;
            }

        }
        else
        {
            error.text = ("Requirements not met");
        }

    }

    public void BotMiddle()
    {
        if (BM1.interactable == true && skillpoints >= 1)
        {
            skillpoints--;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Wizard 1");
            error.text = ("");
            BM1.interactable = false;
            BM2.interactable = true;

        }
        else if (BM2.interactable == true && skillpoints >= 2)
        {
            skillpoints = skillpoints - 2;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Wizard 2");
            error.text = ("");
            BM2.interactable = false;
            BM3.interactable = true;

        }
        else if (BM3.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Wizard 3");
            error.text = ("");
            BM3.interactable = false;
            BM4.interactable = true;

        }
        else if (BM4.interactable == true && skillpoints >= 4)
        {
            skillpoints = skillpoints - 4;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Wizard 4");
            error.text = ("");
            BM4.interactable = false;
            BM5.interactable = true;

        }
        else if (BM5.interactable == true && skillpoints >= 5)
        {
            skillpoints = skillpoints - 5;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Wizard 5");
            error.text = ("");
            BM5.interactable = false;
            BM6.interactable = true;

        }
        else if (BM6.interactable == true && skillpoints >= 6)
        {
            skillpoints = skillpoints - 6;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Wizard 6");
            error.text = ("");
            BM6.interactable = false;
            connectorCheck3++;
            connectorCheck4++;
            if (connectorCheck3 == 2)
            {
                C3.interactable = true;
            }
            if (connectorCheck4 == 2)
            {
                C4.interactable = true;
            }

        }
        else
        {
            error.text = ("Requirements not met");
        }
    }

    public void BotLeft()
    {
        if (BL1.interactable == true && skillpoints >= 1)
        {
            skillpoints--;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Tank 1");
            error.text = ("");
            BL1.interactable = false;
            BL2.interactable = true;

        }
        else if (BL2.interactable == true && skillpoints >= 2)
        {
            skillpoints = skillpoints - 2;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Tank 2");
            error.text = ("");
            BL2.interactable = false;
            BL3.interactable = true;

        }
        else if (BL3.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Tank 3");
            error.text = ("");
            BL3.interactable = false;
            BL4.interactable = true;

        }
        else if (BL4.interactable == true && skillpoints >= 4)
        {
            skillpoints = skillpoints - 4;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Tank 4");
            error.text = ("");
            BL4.interactable = false;
            BL5.interactable = true;

        }
        else if (BL5.interactable == true && skillpoints >= 5)
        {
            skillpoints = skillpoints - 5;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Tank 5");
            error.text = ("");
            BL5.interactable = false;
            BL6.interactable = true;

        }
        else if (BL6.interactable == true && skillpoints >= 6)
        {
            skillpoints = skillpoints - 6;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Tank 6");
            error.text = ("");
            BL6.interactable = false;
            connectorCheck3++;
            if (connectorCheck3 == 2)
            {
                C3.interactable = true;
            }

        }
        else
        {
            error.text = ("Requirements not met");
        }

    }

    public void Middle()
    {
        if (M1.interactable == true && skillpoints >= 1 && (connectorCheck1 == 3 || connectorCheck2 == 3 || connectorCheck3 == 3 || connectorCheck4 == 3))
        {
            skillpoints--;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: ShieldUp");
            error.text = ("");
            M1.interactable = false;
            M2.interactable = true;

        }
        else if (M2.interactable == true && skillpoints >= 2)
        {
            skillpoints = skillpoints - 2;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: BloodThirst");
            error.text = ("");
            M2.interactable = false;
            M3.interactable = true;

        }
        else if (M3.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Revive");
            error.text = ("");
            M3.interactable = false;
            M4.interactable = true;

        }
        else if (M4.interactable == true && skillpoints >= 4)
        {
            skillpoints = skillpoints - 4;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Meditation");
            error.text = ("");
            M4.interactable = false;
            M5.interactable = true;

        }
        else if (M5.interactable == true && skillpoints >= 5)
        {
            skillpoints = skillpoints - 5;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Armor Buff");
            error.text = ("");
            M5.interactable = false;
            M6.interactable = true;

        }
        else if (M6.interactable == true && skillpoints >= 6)
        {
            skillpoints = skillpoints - 6;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Self Healing");
            error.text = ("");
            M6.interactable = false;

        }
        else
        {
            error.text = ("Requirements not met");
        }

    }

    public void Connector1()
    {
        if (C1.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Sneaking");
            error.text = ("");
            C1.interactable = false;
            connectorCheck1++;
        }
    }

    public void Connector2() {
            if (C2.interactable == true && skillpoints >= 3)
            {
                skillpoints = skillpoints - 3;
                skillPointText.text = ("Skill Points: " + skillpoints);
                skillsUnlockedText.text = ("New Skill Unlocked: Bash");
                error.text = ("");
                C2.interactable = false;
                connectorCheck2++;
            }
        }

    public void Connector3() {
            if (C3.interactable == true && skillpoints >= 3)
            {
                skillpoints = skillpoints - 3;
                skillPointText.text = ("Skill Points: " + skillpoints);
                skillsUnlockedText.text = ("New Skill Unlocked: Dash");
                error.text = ("");
                C3.interactable = false;
                connectorCheck3++;
            }
        }

    public void Connector4() { 
        if (C4.interactable == true && skillpoints >= 3)
        {
            skillpoints = skillpoints - 3;
            skillPointText.text = ("Skill Points: " + skillpoints);
            skillsUnlockedText.text = ("New Skill Unlocked: Blocking");
            error.text = ("");
            C4.interactable = false;
            connectorCheck4++;
        }
        
    }
}

